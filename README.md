# Element.in insurance management

This application is shows the solution for the case study.

It solves the problem of managing modular insurance products for customers.
In order to do so, it has the following business entities:

* Customer
* InsuranceContract
* Module

The customer holds the reference data for the customer (name etc.) as well as
a list of insurance contracts.

The insurance contract is the definition of a selection of different modules.
These selected modules are defined with a selected coverage value.
Additionally the insurance contract has a reference to the customer it belongs to
as well as the caluculated price, that is calculated by the system from the
risks of the modules as well as the selected coverage values.

The module itself holds only information about the risk and the mininal and maximal
coverage values.


## UI
Attached you'll find different screenshots that show the UI of the application:

### Login to the system
![Screenshot 1](https://bytebucket.org/mario_david/element-insurance-management/raw/d32c93a7995496eb4e10a8eb08ed87584f5e82b5/img/screens-1.png?token=b513e36df721f901940ecfd0bb7c9148e2a57899)

### Customer edit screen
![Screenshot 2](https://bytebucket.org/mario_david/element-insurance-management/raw/5ff5e74042bd56a52d91b35161c67d9180b7efc8/img/screens-2.png?token=eb572a3fdf4a53cfb35e3bcf0e1223f7c4634f75)

### Insurance Contract screen
![Screenshot 2](https://bytebucket.org/mario_david/element-insurance-management/raw/e8a0e52c358a81d67c81d1b0980552ec5753713b/img/screens-3.png?token=e92f5b9c23dafb4cc015b40fbc36630d7b2e625e)

### Customer edit screen with contracts
![Screenshot 2](https://bytebucket.org/mario_david/element-insurance-management/raw/e8a0e52c358a81d67c81d1b0980552ec5753713b/img/screens-4.png?token=b639c29125e1b6239cce63eae7dd42fd93e7d4f3)

### List of customers in the system
![Screenshot 2](https://bytebucket.org/mario_david/element-insurance-management/raw/e8a0e52c358a81d67c81d1b0980552ec5753713b/img/screens-6.png?token=58b0d80512f6b510ec40093dd2c3bf96de30f0f9)

## technical descriptions

The system is built with the help of "CUBA platform", a framework for rapid application development (of business applications).

It is a web based application, that uses the following technologies:

* Java / Groovy
* Spring
* Vaadin (as a UI layer technology)


The technology has been choosen for different reasons:

* familarity of the framework
* a certain time pressure
* scalability is not a big concern, therefore using a "session-based" application is fine



## Run the application
The application is runnable through a series of gradle commands. It requires a running HSQLDB that is configured in the context.xml of the project.
To make that process as easy as possible, i would instead suggest to download CUBA studio: https://www.cuba-platform.com/download and open the project from there.
You can use a `git clone https://bitbucket.org/mario_david/element-insurance-management` and import this directory through studio.

Running the unit tests can be achieved via `gradlew check`.

## Information about the development process
I used the build pipeline from bitbucket to run the unit tests in a continous manner. This I did after the three hours of work (23/7 - 8pm)
and therefore is not intended to be part the result of the challenge. I just wanted to mentioned this.