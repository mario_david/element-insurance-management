#!/usr/bin/env bash

echo "######################################################################################"
echo "CUBA app-core starting..."
echo "######################################################################################"

echo "Settings:"
env | grep -i CUBA_


: ${CUBA_DB_USERNAME?"CUBA_DB_USERNAME required (docker run -e 'CUBA_DB_USERNAME=myDbUsername)'"}
: ${CUBA_DB_PASSWORD?"CUBA_DB_PASSWORD required (docker run -e 'CUBA_DB_PASSWORD=myPassword)'"}
: ${CUBA_DB_DBMS?"CUBA_DB_DBMS required (docker run -e 'CUBA_DB_DBMS=oracle)'"}
: ${CUBA_DB_JDBC_STRING?"CUBA_DB_JDBC_STRING required (docker run -e 'CUBA_DB_JDBC_STRING=jdbc:oracle:thin:@localhost:1521/XE)'"}


echo "cuba.dbmsType=${CUBA_DB_DBMS}" >> /usr/local/tomcat/conf/catalina.properties
echo "db.username=${CUBA_DB_USERNAME}" >> /usr/local/tomcat/conf/catalina.properties
echo "db.password=${CUBA_DB_PASSWORD}" >> /usr/local/tomcat/conf/catalina.properties

echo "db.jdbcString=${CUBA_DB_JDBC_STRING}" >> /usr/local/tomcat/conf/catalina.properties

echo "cuba.webPort=${CUBA_WEB_PORT}" >> /usr/local/tomcat/conf/catalina.properties


: ${CUBA_WEB_HOST_NAME?"CUBA_WEB_HOST_NAME required (docker run -e 'CUBA_WEB_HOST_NAME=myHostName)'"}

echo "cuba.webHostName=${CUBA_WEB_HOST_NAME}" >> /usr/local/tomcat/conf/catalina.properties

/usr/local/tomcat/bin/catalina.sh run