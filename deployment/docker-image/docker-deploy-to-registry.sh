#!/bin/bash

IMAGE_NAME=$1
IMAGE_VERSION=$2

echo "Image Name: ${IMAGE_NAME}"
echo "Image Version: ${IMAGE_VERSION}"


REGISTRY_HOST=dockerhub.intern.balvi.de

docker tag ${IMAGE_NAME}:${IMAGE_VERSION} ${REGISTRY_HOST}/${IMAGE_NAME}:${IMAGE_VERSION}
#docker push ${REGISTRY_HOST}/${IMAGE_NAME}:${IMAGE_VERSION}


#docker rmi ${IMAGE_NAME}:${IMAGE_VERSION}
#docker rmi ${REGISTRY_HOST}/${IMAGE_NAME}:${IMAGE_VERSION}