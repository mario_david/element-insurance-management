#!/usr/bin/env bash

echo "######################################################################################"
echo "CUBA app starting..."
echo "######################################################################################"

echo "Settings:"
env | grep -i CUBA_



: ${CUBA_CORE_URL_CONNECTION_LIST?"CUBA_CORE_URL_CONNECTION_LIST required (docker run -e 'CUBA_CORE_URL_CONNECTION_LIST=http://my-app-core:8080/app-core,http://my-app-core2:8080/app-core')"}
: ${CUBA_WEB_HOST_NAME?"CUBA_WEB_HOST_NAME required (docker run -e 'CUBA_WEB_HOST_NAME=myHostName)'"}
: ${CUBA_WEB_PORT?"CUBA_WEB_PORT required (docker run -e 'CUBA_WEB_PORT=8080)'"}


echo "cuba.connectionUrlList=${CUBA_CORE_URL_CONNECTION_LIST}" >> /usr/local/tomcat/conf/catalina.properties
echo "cuba.webHostName=${CUBA_WEB_HOST_NAME}" >> /usr/local/tomcat/conf/catalina.properties
echo "cuba.webPort=${CUBA_WEB_PORT}" >> /usr/local/tomcat/conf/catalina.properties


/usr/local/tomcat/bin/catalina.sh run