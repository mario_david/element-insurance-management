#!/bin/bash

APP_NAME=$1
IMAGE_NAME=$2
IMAGE_VERSION=$3

echo "App Name: ${APP_NAME}"
echo "Image Name: ${IMAGE_NAME}"
echo "Image Version: ${IMAGE_VERSION}"

cp build/distributions/war/${APP_NAME}.war deployment/docker-image/${APP_NAME}/container-files/

docker build -t ${IMAGE_NAME}:${IMAGE_VERSION} deployment/docker-image/${APP_NAME}/
