#!/bin/bash

APP_NAME=$1
IMAGE_VERSION=$2

echo "App Name: ${APP_NAME}"
echo "Image Version: ${IMAGE_VERSION}"

./gradlew buildWar
cp build/distributions/war/${APP_NAME}.war deployment/docker-image/${APP_NAME}/container-files/

docker build -t ${APP_NAME}:${IMAGE_VERSION} deployment/docker-image/${APP_NAME}/
