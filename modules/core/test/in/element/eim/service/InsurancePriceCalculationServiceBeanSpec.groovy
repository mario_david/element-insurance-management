package in.element.eim.service

import in.element.eim.entity.InsuranceContract
import in.element.eim.entity.Module
import in.element.eim.entity.SelectedModule
import spock.lang.Specification

class InsurancePriceCalculationServiceBeanSpec extends Specification {

    InsurancePriceCalculationService sut
    Module bikeModule
    Module jewelryModule

    InsuranceContract insuranceContract

    def setup() {

        given: 'the InsurancePriceCalculationServiceBean is the system under test (SUT)'
        sut = new InsurancePriceCalculationServiceBean()

        and: 'there is the bike module with a risk of 30%'
        bikeModule = new Module(name: 'Bike', risk: 30)

        and: 'there is the jewelry module with a risk of 5%'
        jewelryModule = new Module(name: 'Jewelry', risk: 5)
    }

    def "calculatePrice calculates the price for one selected module"() {

        given: 'there is a contract with a single selected module with a selected coverage of 1000 EUR'
        insuranceContract = new InsuranceContract(
                selectedModules: [
                        new SelectedModule(module: bikeModule, selectedCoverage: 1000)
                ]
        )

        and: 'the resulting expected price is 300 EUR'
        def expectedPrice = new BigDecimal(300)

        when: 'the price is calculated'
        def actualPrice = sut.calculatePrice(insuranceContract)

        then: 'the actual price matches the expected price'
        actualPrice == expectedPrice
    }


    def "calculatePrice calculates the price for multiple selected modules"() {

        given: 'there is a contract with two selected modules'
        insuranceContract = new InsuranceContract(
                selectedModules: [
                        new SelectedModule(module: bikeModule, selectedCoverage: 1000),
                        new SelectedModule(module: jewelryModule, selectedCoverage: 5000),
                ]
        )

        and: 'the resulting expected price is 300 EUR (Bike) + 250 (Jewelry)'
        def expectedPrice = new BigDecimal(300 + 250)

        when: 'the price is calculated'
        def actualPrice = sut.calculatePrice(insuranceContract)

        then: 'the actual price matches the expected price'
        actualPrice == expectedPrice
    }

    def "calculatePrice returns 0 if there are no selected modules"() {

        given: 'there is a contract with two selected modules'
        insuranceContract = new InsuranceContract(
                selectedModules: []
        )

        and: 'the resulting expected price is 0 EUR'
        def expectedPrice = new BigDecimal(0)

        when: 'the price is calculated'
        def actualPrice = sut.calculatePrice(insuranceContract)

        then: 'the actual price matches the expected price'
        actualPrice == expectedPrice
    }

}
