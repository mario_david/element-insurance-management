package in.element.eim.listener

import com.haulmont.cuba.core.EntityManager
import in.element.eim.entity.InsuranceContract
import in.element.eim.entity.Module
import in.element.eim.entity.SelectedModule
import in.element.eim.service.InsurancePriceCalculationService
import spock.lang.Specification


class InsuranceContractPriceEntityListenerSpec extends Specification {


    InsuranceContractPriceEntityListener sut
    InsurancePriceCalculationService insurancePriceCalculationService

    InsuranceContract insuranceContract

    def setup() {

        given:
        insurancePriceCalculationService = Mock(InsurancePriceCalculationService)

        and: 'the InsurancePriceCalculationServiceBean is the system under test (SUT) with a mocked version of the insurancePriceCalculationService'
        sut = new InsuranceContractPriceEntityListener(
                insurancePriceCalculationService: insurancePriceCalculationService
        )

        and: 'there is the bike module with a risk of 30%'
        insuranceContract = new InsuranceContract()

    }

    def "onBeforeInsert calculates the price and adds it to the price attribute"() {

        given: 'the mocked price calculation service calcules a price of 500 EUR for this insurance contract'
        def expectedPrice = new BigDecimal(500)
        insurancePriceCalculationService.calculatePrice(insuranceContract) >> expectedPrice


        when: 'the insurance contract is created'
        sut.onBeforeInsert(insuranceContract, Mock(EntityManager))

        then: 'the actual price matches the expected price'
        insuranceContract.price == expectedPrice
    }


    def "onBeforeUpdate calculates the price and adds it to the price attribute"() {

        given: 'the mocked price calculation service calcules a price of 500 EUR for this insurance contract'
        def expectedPrice = new BigDecimal(500)
        insurancePriceCalculationService.calculatePrice(insuranceContract) >> expectedPrice


        when: 'the insurance contract is created'
        sut.onBeforeUpdate(insuranceContract, Mock(EntityManager))

        then: 'the actual price matches the expected price'
        insuranceContract.price == expectedPrice
    }

}
