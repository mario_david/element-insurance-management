create table EIM_MODULE (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255) not null,
    RISK double precision not null,
    MIN_COVERAGE decimal(19, 2) not null,
    MAX_COVERAGE decimal(19, 2) not null,
    --
    primary key (ID)
);
