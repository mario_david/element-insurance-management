create table EIM_SELECTED_MODULE (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    MODULE_ID varchar(36) not null,
    SELECTED_COVERAGE decimal(19, 2) not null,
    INSURANCE_CONTRACT_ID varchar(36) not null,
    --
    primary key (ID)
);
