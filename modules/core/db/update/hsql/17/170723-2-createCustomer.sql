alter table EIM_CUSTOMER add constraint FK_EIM_CUSTOMER_ACCOUNT foreign key (ACCOUNT_ID) references SEC_USER(ID);
create index IDX_EIM_CUSTOMER_ACCOUNT on EIM_CUSTOMER (ACCOUNT_ID);
