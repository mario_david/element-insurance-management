-----------------------------------------------------------------------------
-- Modules
-----------------------------------------------------------------------------

insert into EIM_MODULE
(ID, VERSION, CREATE_TS, CREATED_BY, UPDATE_TS, UPDATED_BY, DELETE_TS, DELETED_BY, NAME, RISK, MIN_COVERAGE, MAX_COVERAGE)
values ('953e4598-ef40-a147-7692-bd2019dc7800', 1, '2017-07-23 11:52:57', 'admin', '2017-07-23 11:52:57', null, null, null, 'Sports Equipment', 30.0, 0.00, 20000.00);
insert into EIM_MODULE
(ID, VERSION, CREATE_TS, CREATED_BY, UPDATE_TS, UPDATED_BY, DELETE_TS, DELETED_BY, NAME, RISK, MIN_COVERAGE, MAX_COVERAGE)
values ('6e35c095-9c6d-b6d7-c095-6b7de7ff6f48', 2, '2017-07-23 11:52:24', 'admin', '2017-07-23 11:53:29', 'admin', null, null, 'Electronics', 35.0, 500.00, 6000.00);
insert into EIM_MODULE
(ID, VERSION, CREATE_TS, CREATED_BY, UPDATE_TS, UPDATED_BY, DELETE_TS, DELETED_BY, NAME, RISK, MIN_COVERAGE, MAX_COVERAGE)
values ('61165988-c6d4-1ee4-67d0-67dc36782b50', 1, '2017-07-23 11:52:08', 'admin', '2017-07-23 11:52:08', null, null, null, 'Jewelry', 5.0, 500.00, 10000.00);
insert into EIM_MODULE
(ID, VERSION, CREATE_TS, CREATED_BY, UPDATE_TS, UPDATED_BY, DELETE_TS, DELETED_BY, NAME, RISK, MIN_COVERAGE, MAX_COVERAGE)
values ('15f22a18-8413-4f72-9e44-0b774e7c96d4', 1, '2017-07-23 11:51:35', 'admin', '2017-07-23 11:51:35', null, null, null, 'Bike', 30.0, 0.00, 3000.00);
