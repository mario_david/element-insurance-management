-- begin EIM_CUSTOMER
create table EIM_CUSTOMER (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255) not null,
    FIRST_NAME varchar(255),
    ACCOUNT_ID varchar(36),
    --
    primary key (ID)
)^
-- end EIM_CUSTOMER
-- begin EIM_MODULE
create table EIM_MODULE (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255) not null,
    RISK double precision not null,
    MIN_COVERAGE decimal(19, 2) not null,
    MAX_COVERAGE decimal(19, 2) not null,
    --
    primary key (ID)
)^
-- end EIM_MODULE
-- begin EIM_INSURANCE_CONTRACT
create table EIM_INSURANCE_CONTRACT (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    CUSTOMER_ID varchar(36) not null,
    CONCLUSION_DATE date not null,
    PRICE decimal(19, 2),
    --
    primary key (ID)
)^
-- end EIM_INSURANCE_CONTRACT
-- begin EIM_SELECTED_MODULE
create table EIM_SELECTED_MODULE (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    MODULE_ID varchar(36) not null,
    SELECTED_COVERAGE decimal(19, 2) not null,
    INSURANCE_CONTRACT_ID varchar(36) not null,
    --
    primary key (ID)
)^
-- end EIM_SELECTED_MODULE
