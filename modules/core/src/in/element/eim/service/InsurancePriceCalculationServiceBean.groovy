package in.element.eim.service

import in.element.eim.entity.InsuranceContract
import in.element.eim.entity.SelectedModule
import org.springframework.stereotype.Service

@Service(InsurancePriceCalculationService.NAME)
class InsurancePriceCalculationServiceBean implements InsurancePriceCalculationService {
    private BigDecimal PRICE_NULL_EUR = new BigDecimal(0)

    BigDecimal calculatePrice(InsuranceContract insuranceContract) {
        if (insuranceContract.selectedModules) {
            insuranceContract.selectedModules.collect { calculateSelectedModulePrice(it) }.sum()
        }
        else {
            PRICE_NULL_EUR
        }
    }

    private BigDecimal calculateSelectedModulePrice(SelectedModule selectedModule) {
        selectedModule.selectedCoverage * (selectedModule.module.risk / 100)
    }
}