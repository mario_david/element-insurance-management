package in.element.eim.listener

import in.element.eim.service.InsurancePriceCalculationService
import org.springframework.stereotype.Component
import com.haulmont.cuba.core.listener.BeforeInsertEntityListener
import com.haulmont.cuba.core.EntityManager
import in.element.eim.entity.InsuranceContract
import com.haulmont.cuba.core.listener.BeforeUpdateEntityListener

import javax.inject.Inject

@Component("eim_InsuranceContractPriceEntityListener")
public class InsuranceContractPriceEntityListener implements BeforeInsertEntityListener<InsuranceContract>, BeforeUpdateEntityListener<InsuranceContract> {

    @Inject
    InsurancePriceCalculationService insurancePriceCalculationService

    @Override
    public void onBeforeInsert(InsuranceContract entity, EntityManager entityManager) {
        calculateInsurancePrice(entity)
    }

    @Override
    public void onBeforeUpdate(InsuranceContract entity, EntityManager entityManager) {
        calculateInsurancePrice(entity)
    }

    private void calculateInsurancePrice(InsuranceContract entity) {
        def calculatedPrice = insurancePriceCalculationService.calculatePrice(entity)
        entity.price = calculatedPrice
    }
}