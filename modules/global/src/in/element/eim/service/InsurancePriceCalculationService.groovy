package in.element.eim.service

import in.element.eim.entity.InsuranceContract


public interface InsurancePriceCalculationService {
    String NAME = "eim_InsurancePriceCalculationService";

    BigDecimal calculatePrice(InsuranceContract insuranceContract)
}