package in.element.eim.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.chile.core.annotations.NamePattern;
import java.math.BigDecimal;
import javax.persistence.Column;

@NamePattern("%s: %s|module,selectedCoverage")
@Table(name = "EIM_SELECTED_MODULE")
@Entity(name = "eim$SelectedModule")
public class SelectedModule extends StandardEntity {
    private static final long serialVersionUID = -1416760947614525616L;

    @Lookup(type = LookupType.DROPDOWN, actions = {"lookup", "clear"})
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "MODULE_ID")
    protected Module module;

    @NotNull
    @Column(name = "SELECTED_COVERAGE", nullable = false)
    protected BigDecimal selectedCoverage;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "INSURANCE_CONTRACT_ID")
    protected InsuranceContract insuranceContract;

    public void setInsuranceContract(InsuranceContract insuranceContract) {
        this.insuranceContract = insuranceContract;
    }

    public InsuranceContract getInsuranceContract() {
        return insuranceContract;
    }


    public void setSelectedCoverage(BigDecimal selectedCoverage) {
        this.selectedCoverage = selectedCoverage;
    }

    public BigDecimal getSelectedCoverage() {
        return selectedCoverage;
    }


    public void setModule(Module module) {
        this.module = module;
    }

    public Module getModule() {
        return module;
    }


}