package in.element.eim.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.chile.core.annotations.NamePattern;

@NamePattern("%s (%s)|name,risk")
@Table(name = "EIM_MODULE")
@Entity(name = "eim$Module")
public class Module extends StandardEntity {
    private static final long serialVersionUID = 3726329768054115569L;

    @NotNull
    @Column(name = "NAME", nullable = false)
    protected String name;

    @NotNull
    @Column(name = "RISK", nullable = false)
    protected Double risk;

    @NotNull
    @Column(name = "MIN_COVERAGE", nullable = false)
    protected BigDecimal minCoverage;

    @NotNull
    @Column(name = "MAX_COVERAGE", nullable = false)
    protected BigDecimal maxCoverage;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setRisk(Double risk) {
        this.risk = risk;
    }

    public Double getRisk() {
        return risk;
    }

    public void setMinCoverage(BigDecimal minCoverage) {
        this.minCoverage = minCoverage;
    }

    public BigDecimal getMinCoverage() {
        return minCoverage;
    }

    public void setMaxCoverage(BigDecimal maxCoverage) {
        this.maxCoverage = maxCoverage;
    }

    public BigDecimal getMaxCoverage() {
        return maxCoverage;
    }


}